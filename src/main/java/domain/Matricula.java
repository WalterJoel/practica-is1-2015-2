package domain;

import javax.persistence.*;

@Entity
public class Matricula implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "matricula_id_generator", sequenceName = "matricula_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "matricula_id_generator")

	private Long matricula_id;


	private Double nota;

	private String semestre;

	/*Representamso la relacion entre alumno y curso aqui en matricula*/
	/*Muchos cursos pueden estar en una matricula*/
	@ManyToOne
	private Curso curso;
	/*Un alumno solo puede tener una matricula por semestre*/
	@OneToOne
	private Alumno alumno;

	@Override
	public Long getId() {
		return matricula_id;
	}

	@Override
	public void setId(Long id) {
		this.matricula_id = id;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Double getNota() {
		return nota;
	}

	public void setNota(Double nota) {
		this.nota = nota;
	}

	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

}